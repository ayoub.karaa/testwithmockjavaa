package com.example.demo.unittest.busnes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.Test;


import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SomeBusnesMockTest {



/*

    @Test
    public void calculateSumUsingDataServiceMock_basic(){
        SomeBusinessImpl busness = new SomeBusinessImpl();
        SomeDataService dataServiceMock =mock(SomeDataService.class);
        when(dataServiceMock.retriveAllData()).thenReturn(new int[]{1,2,3});
        busness.setSomeDataService(dataServiceMock);
        int actualResult = busness.calculateSumUsingDataService();
        int expectedResult = 6 ;
        assertEquals(expectedResult,actualResult);
    }

    @Test
    public void calculateSumUsingDataServiceMock_empty(){
        SomeBusinessImpl busness = new SomeBusinessImpl();
        SomeDataService dataServiceMock =mock(SomeDataService.class);
        when(dataServiceMock.retriveAllData()).thenReturn(new int[]{});
        busness.setSomeDataService(dataServiceMock);
        int actualResult = busness.calculateSumUsingDataService();
        int expectedResult = 0 ;
        assertEquals(expectedResult,actualResult);
    }

    @Test
    public void calculateSumUsingDataServiceMock_oneValue(){
        SomeBusinessImpl busness = new SomeBusinessImpl();
        SomeDataService dataServiceMock =mock(SomeDataService.class);
        when(dataServiceMock.retriveAllData()).thenReturn(new int[]{5});
        busness.setSomeDataService(dataServiceMock);
        int actualResult = busness.calculateSumUsingDataService();
        int expectedResult = 5  ;
        assertEquals(expectedResult,actualResult);
    }

*/

List<String> mock = mock(List.class);

    @InjectMocks
    SomeBusinessImpl business = new SomeBusinessImpl();
    @Mock
    SomeDataService someDataServiceMock = mock(SomeDataService.class);


    @BeforeEach
    public void befor(){
        business.setSomeDataService(someDataServiceMock);
    }

    @Test
    public void calculateSumUsingDataServiceMock_basicBetterCode(){
        when(someDataServiceMock.retriveAllData()).thenReturn(new int[]{1,2,3});
        assertEquals(6,business.calculateSumUsingDataService());
    }

    @Test
    public void calculateSumUsingDataServiceMock_emptyBetterCode(){
        when(someDataServiceMock.retriveAllData()).thenReturn(new int[]{});
        assertEquals(0,business.calculateSumUsingDataService());
    }

    @Test
    public void calculateSumUsingDataServiceMock_oneValueBetterCode(){
        when(someDataServiceMock.retriveAllData()).thenReturn(new int[]{5});
        assertEquals(5,business.calculateSumUsingDataService());
    }


    @Test
    public void verificationBasics(){
        String value1 = mock.get(0);
        String value2 = mock.get(1);

        verify(mock).get(0);
        verify(mock,times(2)).get(anyInt());
        verify(mock,atLeast(1)).get(anyInt());
        verify(mock,atMost(2)).get(anyInt());
        verify(mock,never()).get(2);
    }





}
