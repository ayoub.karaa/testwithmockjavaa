package com.example.demo.unittest.busnes;

import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;
import org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class SomeDataServiceImp implements SomeDataService{

    @Override
    public int[] retriveAllData() {
        return new int[]{1,2,3};
    }
}

public class SomeBusnesTest {

    @Test
    public void calculateSum_basic(){
        SomeBusinessImpl busness = new SomeBusinessImpl();
        int actualResult = busness.calculateSum(new int[]{1,2,3});
        int expectedResult = 6 ;
        assertEquals(expectedResult,actualResult);
    }

   @Test
    public void calculateSumUsingDataService_basic(){
        SomeBusinessImpl busness = new SomeBusinessImpl();
        busness.setSomeDataService(new SomeDataServiceImp());
        int actualResult = busness.calculateSumUsingDataService();
        int expectedResult = 6 ;
        assertEquals(expectedResult,actualResult);
    }


    @Test
    public void calculateSum_Empty(){
        SomeBusinessImpl busness = new SomeBusinessImpl();
        int actualResult = busness.calculateSum(new int[]{});
        int expectedResult = 0 ;
        assertEquals(expectedResult,actualResult);
    }
}
