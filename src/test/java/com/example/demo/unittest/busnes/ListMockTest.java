package com.example.demo.unittest.busnes;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

public class ListMockTest {
    List mock = mock(List.class);

    @Test
    public void test(){
    when(mock.size()).thenReturn(5);
    assertEquals(5,mock.size());
}

    @Test
    public void returnDiffValues(){
    when(mock.size()).thenReturn(5).thenReturn(10);
    assertEquals(5,mock.size());
    assertEquals(10,mock.size());
}

@Test
    public void returnWithParam(){
       // when(mock.get(0)).thenReturn("wiwouw");
        when(mock.get(anyInt())).thenReturn("wiwouw");
        assertEquals("wiwouw", mock.get(0));
       // assertEquals(null, mock.get(1));
        assertEquals("wiwouw", mock.get(1));
}

@Test
    public void argumentCapturing(){
        mock.add("ayouab");
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(mock).add(captor.capture());
        assertEquals("ayouab",captor.getValue());
}

@Test
    public void argumentAllCapturing(){
        mock.add("ayouab1");
        mock.add("ayouab2");
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(mock,times(2)).add(captor.capture());

        List<String> allvalues = captor.getAllValues();
        assertEquals("ayouab1",allvalues.get(0));
}

@Test
    public void mocking(){
    ArrayList arrayListMock = mock(ArrayList.class);
    System.out.println(arrayListMock.get(0));
    System.out.println(arrayListMock.size());
    arrayListMock.add("ayoub1");
    arrayListMock.add("ayoub2");
    arrayListMock.size();
    when(arrayListMock.size()).thenReturn(5);
    System.out.println(arrayListMock.size());
    }

@Test
    public void spying(){
    ArrayList arrayListMock = spy(ArrayList.class);
    arrayListMock.add("ayoub0");
    System.out.println(arrayListMock.get(0));
    System.out.println(arrayListMock.size());
    arrayListMock.add("ayoub1");
    arrayListMock.add("ayoub2");
    arrayListMock.size();
    when(arrayListMock.size()).thenReturn(5);
    System.out.println(arrayListMock.size());
    }


}
