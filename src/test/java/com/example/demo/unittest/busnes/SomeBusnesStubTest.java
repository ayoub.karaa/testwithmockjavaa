package com.example.demo.unittest.busnes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SomeBusnesStubTest {


    @Test
    public void calculateSum_basic(){
        SomeBusinessImpl busness = new SomeBusinessImpl();
        int actualResult = busness.calculateSum(new int[]{1,2,3});
        int expectedResult = 6 ;
        assertEquals(expectedResult,actualResult);
    }


}
